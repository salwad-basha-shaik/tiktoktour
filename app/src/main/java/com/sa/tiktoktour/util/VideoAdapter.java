package com.sa.tiktoktour.util;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.VideoView;
import com.sa.tiktoktour.R;
import com.sa.tiktoktour.model.Video;

import java.util.List;


public class VideoAdapter extends ArrayAdapter<Video> {

    private Activity mContext;
    private List<Video> mVideos;

    public VideoAdapter(@NonNull Activity context, @NonNull List<Video> objects) {
        super(context, R.layout.video_row, objects);

        mContext = context;
        mVideos = objects;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        final ViewHolder holder;


        if (convertView == null) {
            LayoutInflater layoutInflater=mContext.getLayoutInflater();

            convertView=layoutInflater.inflate(R.layout.video_row,null);

//            convertView = LayoutInflater.from(mContext).inflate(
//                    R.layout.video_row, null);
            holder = new ViewHolder();

            holder.videoView = (VideoView) convertView
                    .findViewById(R.id.videoView);

            convertView.setTag(holder);
        } else {

            holder = (ViewHolder) convertView.getTag();

        }

        /***get clicked view and play video url at this position**/
        try {
            Video video = mVideos.get(position);
            //play video using android api, when video view is clicked.
            String url = video.getVideoUrl(); // your URL here
            Uri videoUri = Uri.parse(url);
            holder.videoView.setVideoURI(videoUri);
            holder.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setLooping(false);
                    holder.videoView.start();
                }


            });



        } catch (Exception e) {
            e.printStackTrace();
        }


        return convertView;
    }

    public static class ViewHolder {
        VideoView videoView;

    }
}
