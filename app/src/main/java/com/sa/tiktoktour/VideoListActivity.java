package com.sa.tiktoktour;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.sa.tiktoktour.model.Video;
import com.sa.tiktoktour.util.VideoAdapter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class VideoListActivity extends AppCompatActivity {

    private List<Video> mVideosList = new ArrayList<>();
    private VideoAdapter mVideoAdapter;
    ListView listView;
    private DownloadManager downloadManager;
    private boolean permissionResult=false;
    private final String[] permissionsRequired=new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};
    private static final int PERMISSION_ALL = 1;
    String filepath;
    int position;
    AlertDialog alertDialog;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_list);


        listView = findViewById(R.id.listView1);

        listView.setRecyclerListener(new AbsListView.RecyclerListener() {
            @Override
            public void onMovedToScrapHeap(View view) {
                // Safety net
                VideoView videoView = (VideoView)view.findViewById(R.id.videoView);
                videoView.stopPlayback();
            }
        });

        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        askPermission();

        mVideosList.add(new Video("https://s3.ap-south-1.amazonaws.com/s3.demo.2/tiktok1.mp4"));
        mVideosList.add(new Video("https://s3.ap-south-1.amazonaws.com/s3.demo.2/tiktok2.mp4"));
        mVideosList.add(new Video("https://s3.ap-south-1.amazonaws.com/s3.demo.2/tiktok3.mp4"));
        mVideosList.add(new Video("https://s3.ap-south-1.amazonaws.com/s3.demo.2/tiktok4.mp4"));
        mVideosList.add(new Video("https://s3.ap-south-1.amazonaws.com/s3.demo.2/tiktok5.mp4"));
        mVideosList.add(new Video("https://s3.ap-south-1.amazonaws.com/s3.demo.2/tiktok6.mp4"));
        mVideosList.add(new Video("https://s3.ap-south-1.amazonaws.com/s3.demo.2/tiktok7.mp4"));
        mVideosList.add(new Video("https://s3.ap-south-1.amazonaws.com/s3.demo.2/tiktok8.mp4"));

        mVideoAdapter = new VideoAdapter(this, mVideosList);
        listView.setAdapter(mVideoAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l)
            {
                position=pos;
                Log.e("position",""+pos);

                new DownloadAndShareTask(mVideosList.get(pos).getVideoUrl()).execute();
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void askPermission() {
        for(String permission:permissionsRequired){
            if(ActivityCompat.checkSelfPermission(this,permission)!= PackageManager.PERMISSION_GRANTED){
                requestPermissions(permissionsRequired,PERMISSION_ALL);
                return;
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode)
        {
            case 1:{
                if(grantResults.length>0&&grantResults[0]== PackageManager.PERMISSION_GRANTED
                        &&grantResults[1]==PackageManager.PERMISSION_GRANTED
                        ){
                    permissionResult=true;

                }
                else
                {
                    permissionResult=false;
//                    Toast.makeText(getApplicationContext(),"this app must need permissions",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void movetotest(View view)
    {
        Log.e("test:position","is"+position);
    }

    private void shareToWhatsApp() {

        Log.e("uridestination",filepath);
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image=null;
        try {
            image= new File(filepath);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String type = "video/*";

        // Create the new Intent using the 'Send' action.
        Intent share = new Intent(Intent.ACTION_SEND);

        // Set the MIME type
        share.setType(type);

        // Create the URI from the media
        Log.e("filPath",image.toString());

        Uri uri = FileProvider.getUriForFile(VideoListActivity.this, getPackageName() +".provider", image);
        share.setPackage("com.whatsapp");

        // Add the URI to the Intent.
//        share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:/"+fileToDownload));
        share.putExtra(Intent.EXTRA_STREAM, uri);
        share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        PackageManager packageManager = getPackageManager();
        if (share.resolveActivity(packageManager) != null) {
            startActivity(share);
            startActivity(Intent.createChooser(share, "Share to"));
        } else {
            Toast.makeText(this, "please install whattsapp!", Toast.LENGTH_SHORT).show();
//            alertForApp(getString(R.string.install_whatsapp), "com.whatsapp");
        }
    }


    class DownloadAndShareTask extends AsyncTask<Void, Void, Boolean> {
        String url;
        public DownloadAndShareTask(String url)
        {
            this.url =url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            callGlowingLoader();
            Log.e("AsynTask","onpreexecute.");
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            Uri uri= Uri.parse(url);
            DownloadManager.Request request = new DownloadManager.Request(uri);
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
            request.setAllowedOverRoaming(false);
            request.setTitle("Downloading " + "se_down_mine.mp4");
            request.setVisibleInDownloadsUi(true);
            request.allowScanningByMediaScanner();
            File image=null;
            try {
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                filepath="/storage/emulated/0/Pictures/MY/tiktok_"+timeStamp+".mp4";
                image= new File(filepath);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Uri uri3;
            Log.e("uridestination",filepath);
            uri3=  Uri.fromFile(image);
            request.setDestinationUri(uri3);
            final long refId = downloadManager.enqueue(request);
            Log.e("OUT", "" + refId);
            Log.e("AsynTask","Task Completed.");
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            alertDialog.dismiss();

            new Handler().postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    shareToWhatsApp();
                }
            }, 2000);



            Log.e("AsynTask","Restart");
        }
    }


    private void callGlowingLoader()
    {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.prompts_order_glowingloader, null);
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
//        userInput = promptsView.findViewById(R.id.editTextDialogUserInput2);
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


}
